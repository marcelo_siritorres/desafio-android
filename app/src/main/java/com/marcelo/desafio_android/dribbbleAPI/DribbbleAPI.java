package com.marcelo.desafio_android.dribbbleAPI;


import com.marcelo.desafio_android.Model.Shot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DribbbleAPI {

    @GET("shots")
    Call<List<Shot>> getDribbbles(@Query("access_token") String apiKey, @Query("timeframe") String time, @Query("page") int page);

}
