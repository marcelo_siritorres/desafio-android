package com.marcelo.desafio_android.MainActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.marcelo.desafio_android.Model.Shot;
import com.marcelo.desafio_android.R;
import com.marcelo.desafio_android.dribbbleAPI.ApiClient;
import com.marcelo.desafio_android.dribbbleAPI.DribbbleAPI;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final  String API_KEY = "b019f547bc0b28d7ffef0e10f7be44c0c519535e5e57595e03d73d31a9177688";

    @BindView(R.id.rcViewDribbble)
    public RecyclerView rcViewDribbble;

    public DribbbleAPI dribbbleAPI;

    public ListAdapter listAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rcViewDribbble = (RecyclerView) findViewById(R.id.rcViewDribbble);
        rcViewDribbble.setLayoutManager(new LinearLayoutManager(this));

        dribbbleAPI = ApiClient.getClient().create(DribbbleAPI.class);

        Call<List<Shot>> call = dribbbleAPI.getDribbbles(API_KEY, "week", 1);

        Log.e("URL", call.request().url().toString());


//    OBS: Suspeito que a API Dribbbles está enviando o dado em vários 'Shots' (mais de uma 'emissão')
//         fora de uma estrutura possível de ser convertida em List pertencente a uma classe
//         Talvez, para pega-los eu devesse usar Observables.  Mas infelizmente não deu tempo no prazo estipulado.
//         Segue minha URL de teste:  http://api.dribbble.com/v1/shots?access_token=b019f547bc0b28d7ffef0e10f7be44c0c519535e5e57595e03d73d31a9177688&timeframe=week&page=1


        call.enqueue(new Callback<List<Shot>>() {
            @Override
            public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {

                int statusCode = response.code();
                List<Shot> shots = response.body();

//                if(response.body() != null && response.body().size()> 0){
//                    rcViewDribbble.setAdapter(new ListAdapter(shots, R.layout.list_item_shots));
//                }

                listAdapter = new ListAdapter(shots, R.layout.list_item_shots);
                rcViewDribbble.setAdapter(new ListAdapter(shots, R.layout.list_item_shots));

                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Shot>> call, Throwable t) {

                Log.e(TAG, t.toString());
            }
        });
    }


    public void onResume() {
        super.onResume();
    }
}