package com.marcelo.desafio_android.MainActivity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marcelo.desafio_android.Model.Shot;
import com.marcelo.desafio_android.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Marcelo on 12/07/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ShotViewHolder> {

    private List<Shot> shots;
    private int rowLayout;



    public ListAdapter(List<Shot> shots, int rowLayout) {
        this.shots = shots;
        this.rowLayout = rowLayout;
    }

    @Override
    public ListAdapter.ShotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ShotViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ShotViewHolder holder, final int position) {

        holder.setDados( shots.get( position ) );
    }

    @Override
    public int getItemCount() {

        return shots.size();
    }


    ////------------------------------HOLDER----------------------------------------------


    public static class ShotViewHolder extends RecyclerView.ViewHolder {

        ImageView imgShotAPI;
        TextView txtTitleAPI;
        TextView txtArtistaAPI;


        public ShotViewHolder(View v) {
            super(v);
            txtTitleAPI = (TextView) v.findViewById(R.id.txtTitleAPI);
            txtArtistaAPI = (TextView) v.findViewById(R.id.txtArtistaAPI);
            imgShotAPI = (ImageView) v.findViewById(R.id.imgShotAPI);
        }

        private void setDados( Shot shot ){

            Picasso.with( imgShotAPI.getContext() )
                    .load( shot.getImages().getTeaser() )
                    .into( imgShotAPI );

            txtTitleAPI.setText( shot.getTitle() );
            txtArtistaAPI.setText( shot.getCreatedAt() );
        }
    }
}
